# Ansible CISCO

The objective is to Automatize setup configuration and checking BGP on CISCO 888.

## Authors

| LASTNAME  | FIRSTNAME |
| --------- | --------- |
| CARRASSET | Maxime    |

# Prerequisite

A LINUX instance for launch ANSIBLE script

# Install Ansible

| Debian               |        | Ubuntu                | 
| -------------------- | ------ | --------------------- | 
| Debian 11 (Bullseye) |   ->   | Ubuntu 20.04 (Focal)  | 
| Debian 10 (Buster)   |   ->   | Ubuntu 18.04 (Bionic) | 
| Debian 9 (Stretch)   |   ->   | Ubuntu 16.04 (Xenial) | 
| Debian 8 (Jessie)    |   ->   | Ubuntu 14.04 (Trusty) |

Add the following line to ```/etc/apt/sources.list``` or ```/etc/apt/sources.list.d/ansible.list```:

```bash
deb http://ppa.launchpad.net/ansible/ansible/ubuntu MATCHING_UBUNTU_CODENAME_HERE main
```

Example for Debian 11 (Bullseye)

```bash
deb http://ppa.launchpad.net/ansible/ansible/ubuntu focal main
```

Then run these commands:
```bash
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
sudo apt update
sudo apt install ansible
```

## Install CISCO plugin 

This plugin is part of the cisco.ios collection (version 2.6.0).

You might already have this collection installed if you are using the ```ansible``` package. It is not included in ```ansible-core```. To check whether it is installed, run ```ansible-galaxy collection list```.

To install it, use: ```ansible-galaxy collection install cisco.ios```.

To use it in a playbook, specify: ```cisco.ios.ios_command```.

 Please refer to Ansible official Documentation on *[docs.ansible.com](https://docs.ansible.com/ansible/latest/collections/cisco/ios/ios_command_module.html#ansible-collections-cisco-ios-ios-command-module)*

 ### Exemple

 ```yaml
 - name: run show version on remote devices
  cisco.ios.ios_command:
    commands: show version

- name: run show version and check to see if output contains IOS
  cisco.ios.ios_command:
    commands: show version
    wait_for: result[0] contains IOS

- name: run multiple commands on remote nodes
  cisco.ios.ios_command:
    commands:
    - show version
    - show interfaces

- name: run multiple commands and evaluate the output
  cisco.ios.ios_command:
    commands:
    - show version
    - show interfaces
    wait_for:
    - result[0] contains IOS
    - result[1] contains Loopback0

- name: run commands that require answering a prompt
  cisco.ios.ios_command:
    commands:
    - command: 'clear counters GigabitEthernet0/1'
      prompt: 'Clear "show interface" counters on this interface \[confirm\]'
      answer: 'y'
    - command: 'clear counters GigabitEthernet0/2'
      prompt: '[confirm]'
      answer: "\r"
```

